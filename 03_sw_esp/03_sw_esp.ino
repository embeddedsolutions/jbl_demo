/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#include "wifi.h"
#include "dbg.h"
#include "common.h"
#include "rest.h"
#include "uart.hpp"

extern Wifi* wifi;
Rest rest;
Uart uart;
uint8_t rxBuff[32];


void setup() {
  delay(50);
  Serial.begin(SERIAL_BAUDRATE); /* Open serial communications and wait for port to open */
  while (!Serial);

  Serial.printf("\n[SYSTEM] ver %d.%d, built %s\n", SW_VERSION_MAJOR, SW_VERSION_MINOR, RELEASE_DATE);
  Serial.printf("\tboot ver %d, ", system_get_boot_version());
  Serial.printf("userbin addr %x\n", system_get_userbin_addr());
  Serial.printf("\tboot mode %d, ", system_get_boot_mode());
  Serial.printf("CPU freq %d\n", system_get_cpu_freq());
  Serial.printf("\tflash map %d, ", system_get_flash_size_map());
  Serial.printf("SDK ver %s\n", system_get_sdk_version());
  Serial.printf("\tfree heap %d B, ", system_get_free_heap_size());
  Serial.printf("upgrade flag %d\n", system_upgrade_flag_check());
  Serial.printf("\trst reason %d exccause %d\n", system_get_rst_info()->reason, system_get_rst_info()->exccause);
  Serial.printf("\tMAC %s\n", Wifi::getMAC().c_str());

  wifi = new Wifi();
  rest.init();
}

void loop() {
  wifi->loop();
  rest.handle_client();
  static_cast<void>(uart.process(rxBuff));
  static_cast<void>(rxBuff);
}
