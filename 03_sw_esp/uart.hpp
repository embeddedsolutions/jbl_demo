/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/
#ifndef UART_HPP_
#define UART_HPP_

#include <Arduino.h>
#include "crc8.hpp"
#include "common.h"

/** Max allowed packet length */
#define MAX_PACKET_LEN (0x07u)

/** Checksum length */
#define CHECKSUM_LEN (0x02u)

/** Command ID + packet LEN offset */
#define HEADER_OFFSET (0x02u)

/** Payload's max single byte value */
#define MAX_PAYLOAD_BYTE_VAL (0x0Fu)

#define PWM_CHANNELS_NUM (5)
#define BTNS_NUM (3)

struct BtnValues {
  uint32_t btn1;
  uint32_t btn2;
  uint32_t btn3;
};

struct Colors {
  uint16_t r;
  uint16_t g;
  uint16_t b;
  uint16_t w;
  uint16_t y;
  uint16_t t;
};

class Uart
{
  public:
    enum STATE {
      GET_BTN_ID_RECV, /**<State command ID received */
      LEN_RECV,        /**<State length received */
      PAYLOAD_RECV     /**<State payload received */
    };

    enum CMD {
      HNDSHAKE = 0x81,       /**<Handshake command id */
      SET_PWM = 0xF1,       /**<Set PWM command id */
      GET_BTN_STATE = 0xE1  /**<Send Buttons state command id */
    };

    Uart(): currState(GET_BTN_ID_RECV) {}
    void updateColor(uint16_t r, uint16_t g, uint16_t b, uint16_t w, uint16_t y, uint16_t t);
    void sendLightsMessage();
    void sendHandshakeMessage();
    uint8_t process(uint8_t *pBuf);
    BtnValues getBtnValues();
    Colors getColorsValues();
    /**
     * @brief Method for converting 2 bytes into uint8_t
     * @param fByte - first byte of the data
     * @param sByte - second byte of the data
     * @retval converted uint8_t value
     */
    uint8_t TwoBytesIntoUint8(uint8_t fByte, uint8_t sByte);

  private:
    Colors colors;
    BtnValues btnValues;
    Crc8 crc;
    STATE currState;
    uint8_t dataBuf[MAX_PACKET_LEN + CHECKSUM_LEN];

    void setNuvotonPacket(uint8_t* buff, uint8_t cmd, boolean ack, Colors colors);
    void updateButtonState(uint8_t* rsp);
    bool isValidFrame(uint8_t* rsp);
    bool crcIsValid(uint8_t *pBuf, uint8_t len, uint8_t rxCrc);
};

#endif /* UART_HPP_ */