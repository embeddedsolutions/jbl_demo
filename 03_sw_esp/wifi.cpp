/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#include "wifi.h"
#include "dbg.h"

#define DEFAULT_AP_PASS ("12345678")
#define WIFI_TIMEOUT_MS (300)
Wifi* wifi = NULL;

Wifi::Wifi()
{
    this->state = WIFI_STATE_AP_SETUP;
    this->_ap_ssid = DEFAULT_DEV_NAME;
    this->_ap_pass = DEFAULT_AP_PASS;
    wifi_station_set_hostname(DEFAULT_DEV_NAME.c_str());
    WiFi.hostname(DEFAULT_DEV_NAME.c_str());
}

void Wifi::setup_AP()
{
  WiFi.disconnect();
  delay(WIFI_TIMEOUT_MS);
  wifi_station_set_hostname(this->_ap_ssid.c_str());
  WiFi.hostname(this->_ap_ssid.c_str());
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(IPAddress(192, 168, 4, 1), IPAddress(192, 168, 4, 1), IPAddress(255, 255, 255, 0));

  const int channel = 1;
  const int ssid_hidden = 0;
  const int max_connections = 8;

  WiFi.softAP(this->_ap_ssid.c_str(), this->_ap_pass.c_str(), channel, ssid_hidden, max_connections);
  DEBUG_SERIAL("[WIFI] AP name: ");
  DEBUG_SERIAL(this->_ap_ssid.c_str());
  DEBUG_SERIAL(", pass: ");
  DEBUG_SERIAL_LN(this->_ap_pass.c_str());
}

String Wifi::getMAC()
{
  return WiFi.macAddress();;
}

void Wifi::loop()
{
  switch(this->state)
  {

    case WIFI_STATE_AP_SETUP:
    {
      DEBUG_SERIAL_LN("[WIFI] AP setup");
      this->setup_AP();
      this->state = WIFI_STATE_AP_IDLE;
      break;
    }

    case WIFI_STATE_AP_IDLE:
    {
      if(WiFi.softAPgetStationNum() > 0){
        this->state = WIFI_STATE_AP_CONNECTED;
        DEBUG_SERIAL_LN("[WIFI] Device connected to AP");
      }
      break;
    }

     case WIFI_STATE_AP_CONNECTED:
    {
      if(WiFi.softAPgetStationNum() == 0)
      {
        this->state = WIFI_STATE_AP_IDLE;
        DEBUG_SERIAL_LN("[WIFI] All devices disconnected from AP");
      }
      break;
    }

    default:
    {
      DEBUG_SERIAL_LN("[WIFI] Critical error - Wifi in unknown state.");
      this->state = WIFI_STATE_AP_SETUP;
      break;
    }
  }
}
