# README #
This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

#### Quick summary ####
This repository keeps source code for Solar Control Test project. This project runs on ESP8266 target MCU using Arduino environment for development.

#### Version ####
* Version of Arduino IDE (tested as stable): 1.8.13.
* Version of ESP8266 board: NodeMCU 1.0

## How do I get set up? ##

#### Board Manager configuration ####
Board package from updated Board Manager should be installed. Version 2.7.2 of ESP8266 was tested as stable.

#### Board configuration ####
Choose OLIMEX MOD_WIFI ESP8266n in Arduino IDE.
FLash Size:4MB FS:2MB OTA:1019KB
CPU Frequency: 160Mhz

#### Library Manager configuration ####
Following  libraries should be installed with Library Manager:
* ArduinoJson - version 6.19.2 tested as stable.
* CRC32 - version 2.0.0 tested as stable.

