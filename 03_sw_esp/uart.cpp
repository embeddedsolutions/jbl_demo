/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#include "uart.hpp"
#include "dbg.h"
 
#define CH_IDX_R (0)
#define CH_IDX_G (1)
#define CH_IDX_B (2)
#define CH_IDX_W (3)
#define CH_IDX_Y (4)

#define NOVUTON_SET_COLOR (0x01)

void Uart::updateColor(uint16_t r, uint16_t g, uint16_t b, uint16_t w, uint16_t y, uint16_t t){
    colors.r = r;
    colors.g = g;
    colors.b = b;
    colors.w = w;
    colors.y = y;
    colors.t = t;
}

BtnValues Uart::getBtnValues() {
    return btnValues;
}

Colors Uart::getColorsValues() {
    return colors;
}

/* For detailed description check the included header file */
uint8_t Uart::TwoBytesIntoUint8(uint8_t fByte, uint8_t sByte) {
  return ((fByte << 4) | sByte);
}

/* For detailed description check the included header file */
bool Uart::crcIsValid(uint8_t *pBuf, uint8_t len, uint8_t rxCrc) {

  if ((!pBuf) || (len <= 0u))
    return false;

  uint8_t calculatedCrc = crc.crc8xFast(pBuf, len);
  return (calculatedCrc == rxCrc);
}

uint8_t Uart::process(uint8_t *pBuf){

  uint8_t readByte;
  static uint8_t dataLen;
  static uint8_t tmpDataLen;
  static uint8_t idx;

  if ((!Serial.available()) || (!pBuf))
    return 0;

  switch(currState) {

    /* Receive command ID */
    case STATE::GET_BTN_ID_RECV: {
      readByte = Serial.read();

      if (CMD::GET_BTN_STATE == readByte) {
        Serial.print("[RX] packet: 0x");
        Serial.print(readByte, HEX);
        Serial.print(" ");

        idx = 0x00;
        dataBuf[idx++] = readByte;
        currState = STATE::LEN_RECV;
      }
      break;
    }

    /* Receive data length */
    case STATE::LEN_RECV: {
      dataLen = Serial.read();
      tmpDataLen = dataLen;

      Serial.print("0x");
      Serial.print(tmpDataLen, HEX);

      if (tmpDataLen != MAX_PACKET_LEN) {
        Serial.println();
        Serial.print("[RX] decoding length error. Expected len: 0x");
        Serial.print(MAX_PACKET_LEN, HEX);
        Serial.print(" received len: 0x");
        Serial.println(tmpDataLen, HEX);
        currState = STATE::GET_BTN_ID_RECV;
      }
      else {
        dataBuf[idx++] = dataLen;
        currState = STATE::PAYLOAD_RECV;
      }

      break;
    }
    
    /* Receive data and verify the checksum */
    case STATE::PAYLOAD_RECV: {
      if ((tmpDataLen ) > 0) {
        readByte = Serial.read();

        if (readByte > MAX_PAYLOAD_BYTE_VAL) {
          Serial.println(" -> payload error");
          currState = STATE::GET_BTN_ID_RECV;
          return 0;
        }

        Serial.print(" 0x");
        Serial.print(readByte, HEX);
        Serial.print(" ");

        dataBuf[idx++] = readByte;
        tmpDataLen--;
      }

      if (tmpDataLen == 0) {
        Serial.println();
        currState = STATE::GET_BTN_ID_RECV;

        uint8_t rxCrc = TwoBytesIntoUint8(dataBuf[MAX_PACKET_LEN], dataBuf[MAX_PACKET_LEN + 0x01u]);

        if (!crcIsValid(dataBuf, dataLen, rxCrc)) {
          Serial.println("[RX] crc error");
          return 0;
        }

        uint8_t tmpBntVal[BTNS_NUM];

        for (uint8_t i = 0; i < BTNS_NUM; i++) 
            tmpBntVal[i] = dataBuf[HEADER_OFFSET + i];

        Serial.print("[RX] sw1:0x");
        Serial.print(tmpBntVal[0], HEX);
        Serial.print(" sw2:0x");
        Serial.print(tmpBntVal[1], HEX);
        Serial.print(" sw3:0x");
        Serial.print(tmpBntVal[2], HEX);
        Serial.println();

        uint8_t randomNr = TwoBytesIntoUint8(dataBuf[HEADER_OFFSET + BTNS_NUM], dataBuf[HEADER_OFFSET + BTNS_NUM + 0x01u]);

        if (tmpBntVal[0]) {
            btnValues.btn1 = randomNr;
        } else {
            btnValues.btn1 = 0;
        }

        if (tmpBntVal[1]) {
            btnValues.btn2 = randomNr;
        } else {
            btnValues.btn2 = 0;
        }

        if (tmpBntVal[2]) {
            btnValues.btn3 = randomNr;
        } else {
            btnValues.btn3 = 0;
        }

        Serial.print("[RX] random nr: 0x");
        Serial.print(randomNr, HEX);
        Serial.println();

        memcpy(pBuf, &dataBuf[HEADER_OFFSET], dataLen - CHECKSUM_LEN);
        return dataLen - CHECKSUM_LEN;
      }
   
      break;
    }
  }

  return 0;
}

void Uart::sendLightsMessage(){
    DEBUG_SERIAL_LN("[UART] Send lights message called");
    const uint8_t packetLen = 22;
    uint8_t cmdTab[packetLen] = {0,};

    setNuvotonPacket(cmdTab, NOVUTON_SET_COLOR, false, colors);
    Serial.write(cmdTab, packetLen);
    Serial.flush();
}

void Uart::sendHandshakeMessage(){
    Serial.write(CMD::HNDSHAKE);
    Serial.flush();
}

void Uart::setNuvotonPacket(uint8_t* buff, uint8_t cmd, boolean ack, Colors colors)
{
    uint8_t cmdParams[] = {
        0xf0 | (0x0f & cmd),  15 + 3 + 2,
        0xF & colors.w>>8, 0xF & colors.w>>4, 0xF & colors.w,
        0xF & colors.y>>8, 0xF & colors.y>>4, 0xF & colors.y,
        0xF & colors.r>>8, 0xF & colors.r>>4, 0xF & colors.r,
        0xF & colors.g>>8, 0xF & colors.g>>4, 0xF & colors.g,
        0xF & colors.b>>8, 0xF & colors.b>>4, 0xF & colors.b,
        0xF & colors.t>>8, 0xF & colors.t>>4, 0xF & colors.t,
        0, 0
    };
 
    if(ack){
        cmdParams[0] = 0x80 | (0x0f & cmd);
    }
 
    uint8_t calculatedCrc = crc.crc8xFast(cmdParams, 20);
    cmdParams[20] = 0xF & calculatedCrc >> 4;
    cmdParams[21] = calculatedCrc & 0xF;
 
    memcpy(buff, cmdParams, 22);
}