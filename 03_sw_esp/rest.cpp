/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#include <ArduinoJson.h>
#include "ESP8266WebServer.h"

#include "rest.h"
#include "common.h"
#include "index.h"
#include "dbg.h"
#include "uart.hpp"

#define HTTP_PORT  (80)
#define HTTP_SUCCESS  (200)
#define HTTP_ACCEPTED (202)
#define HTTP_ERROR    (400)

static ESP8266WebServer server(HTTP_PORT);

extern Uart uart;

static void handleBtn() {
    BtnValues btnValues = uart.getBtnValues();

    String btnNumber = "1";
    if(server.args() > 0){
        if(server.argName(0) == "btn"){
            btnNumber = server.arg(0);
        } else {
            server.send(500, "text/plane", "Invalid arguments!");
            return;
        }
    }

    int btnNumberInt = atoi(btnNumber.c_str());

    String btnValue;

    switch(btnNumberInt){
        case 1:
            btnValue = String(btnValues.btn1);
            break;
        case 2:
            btnValue = String(btnValues.btn2);
            break;
        case 3:
            btnValue = String(btnValues.btn3);
            break;
    }
    server.send(200, "text/plane", btnValue);
}

static void handleMainPage(){
    DEBUG_SERIAL_LN("[REST] Called main page");
    server.send(200, "text/html", MAIN_page);
}

static void handleHandshake() { 
    DEBUG_SERIAL_LN("[REST] Handshake button pressed");
    uart.sendHandshakeMessage();
    server.send(200, "text/plane", "HandShake OK");
}

static void sendColor() { 
    DEBUG_SERIAL_LN("[REST] Send color form filled");
    String rStr,gStr,bStr,wStr,yStr,tStr;

    for (int i = 0; i < server.args(); i++) {
        if(server.argName(i) == "r"){
            rStr = server.arg(i);
        } else if(server.argName(i) == "g"){
            gStr = server.arg(i);
        } else if(server.argName(i) == "b"){
            bStr = server.arg(i);
        } else if(server.argName(i) == "w"){
            wStr = server.arg(i);
        } else if(server.argName(i) == "y"){
            yStr = server.arg(i);
        } else if(server.argName(i) == "t"){
            tStr = server.arg(i);
        }
    }

    int r = atoi(rStr.c_str());
    int g = atoi(gStr.c_str());
    int b = atoi(bStr.c_str());
    int w = atoi(wStr.c_str());
    int y = atoi(yStr.c_str());
    int t = atoi(tStr.c_str());

    t = (t < 0) ? 0 : t;
    t = (t > 255) ? 255 : t;

    uart.updateColor(r, g, b, w, y, t);
    uart.sendLightsMessage();
    server.send(200, "text/plane", "Lights turn on");
}

void Rest::init(){
    server.on("/",          HTTP_GET, handleMainPage);
    server.on("/handshake", HTTP_GET, handleHandshake);
    server.on("/sendColor", HTTP_POST, sendColor);
    server.on("/readBtn",   HTTP_GET, handleBtn);

    server.begin();
    DEBUG_SERIAL_LN("[REST] HTTP server started on port: "  + HTTP_PORT);
}

void Rest::deinit(){
    server.stop();
}

void Rest::handle_client(){
    server.handleClient();
}
