/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#ifndef DBG_H_
#define DBG_H_

/***************************************************
 * INCLUDES
 ***************************************************/
#include <Arduino.h>

/***************************************************
 * DEFINES
 ***************************************************/
#define SERIAL_BAUDRATE 115200

void debug_enable();
void debug_disable();
bool debug_enabled();

#define DEBUG_SERIAL_LN(text)                 if(debug_enabled()) Serial.println(text)
#define DEBUG_SERIAL_LN_FORMAT(text, format)  if(debug_enabled()) Serial.println(text, format)
#define DEBUG_SERIAL(text)                    if(debug_enabled()) Serial.printf(text)

#endif /* DBG_H_ */
