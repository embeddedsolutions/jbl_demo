/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#include "uart.hpp"


const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <title>Serial commands demo</title>
  <style>
    html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}
    body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}
    .button {display: block;width: 160px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}
    p {font-size: 14px;color: #888;margin-bottom: 10px;}
  </style>
</head>

<body>
  <h1>The ESP8266 Nuvoton communication POC</h1>

  </div>
    <button id="handshake" onclick="sendHandshake()" style="border: none;display: block;width:50%;background-color:#34495e;color:white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px" type="button">Handshake</button>
  <div>

  <div class="btn-group" style="width:100%">
    BTN1: <button id="Btn1Value" style="background-color:#34495e;color:white;width:15%" disabled>0</button>
    BTN2: <button id="Btn2Value" style="background-color:#34495e;color:white;width:15%" disabled>0</button>
    BTN3: <button id="Btn3Value" style="background-color:#34495e;color:white;width:15%" disabled>0</button>
  </div>

  <div>
    <p>Send selected color:</p>
    <form id="colorForm" onsubmit="sendColor(this);return false"  method="POST">
      <div>Red: <input type="range" min="0" max="2400" name="r" value="0"></div>
      <div>Green: <input type="range" min="0" max="2400" name="g" value="0"></div>
      <div>Blue: <input type="range" min="0" max="2400" name="b" value="0"></div>
      <div>White: <input type="range" min="0" max="2400" name="w" value="0"></div>
      <div>Yellow: <input type="range" min="0" max="2400" name="y" value="0"></div>
      <div>Timeout: <input type="text" name="t" value="0"></div>
      <div><button id="form_send" style="background-color:#34495e;color:white;width:50%" type="submit">Send</button></div>
    </form>
    <br>
  </div>

  <script>
    setInterval(function() {
      getData(1);
      getData(2);
      getData(3);
    }, 500); //update rate in ms

    function getData(btn) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var elem = document.getElementById("Btn" + btn + "Value");
          elem.innerHTML = this.responseText;
          if(this.responseText !== "0") {
            elem.style.backgroundColor = "#16a085";
            elem.style.color = "black";
          } else {
            elem.style.backgroundColor = "#34495e";
            elem.style.color = "white";
          }
        }
      };
      xhttp.open("GET", "readBtn?btn=" + btn, true);
      xhttp.send();
    }

    function sendHandshake(led) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var elem = document.getElementById("handshake");
          elem.style.backgroundColor = "#16a085";
          elem.style.color = "black";
        }
      };
      xhttp.open("GET", "/handshake", true);
      xhttp.send();
    }

    function sendColor(form) {
      console.log("Sending form");
      console.log(form);
      console.log(form.r);
      console.log(form.r.value);

      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var elem = document.getElementById("handshake");
          // elem.style.backgroundColor = "#34495e";
          // elem.style.color = "white";
        }
      };
      xhttp.open("POST", "/sendColor?r=" + form.r.value + "&g=" + form.g.value + "&b=" + form.b.value + "&w=" + form.w.value + "&y=" + form.y.value + "&t=" + form.t.value, true);
      xhttp.send();
    }
  </script>

  <br><br><a href="https://jbl.de">jbl.de</a>

</body>
</html>
)=====";