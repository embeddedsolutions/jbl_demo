/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#include "dbg.h"

static bool dbg_enabled = true;

void debug_enable(){
    dbg_enabled = true;
}

void debug_disable(){
    dbg_enabled = false;
}

bool debug_enabled(){
    return dbg_enabled;
}