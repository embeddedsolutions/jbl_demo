/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#include "stdint.h"

#define SW_VERSION_MAJOR 0
#define SW_VERSION_MINOR 1

#define SW_VERSION (((SW_VERSION_MAJOR) << 8) | (SW_VERSION_MINOR))
#define MANUFACTURER_NAME "JBL"
#define RELEASE_DATE "20.02.2022"

#endif
