/***************************************************
 * Project name: JBL ESP Test
 *
 * Created by: Embedded Solutions
 * www.embeddedsolutions.pl
 *
 ***************************************************/

#ifndef WIFI_H_
#define WIFI_H_

#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

#define DEFAULT_DEV_NAME ("JBL_" + Wifi::getMAC().substring(12, 14) + Wifi::getMAC().substring(15, 17))

typedef enum
{
  /* Config states */
  WIFI_STATE_AP_SETUP,
  WIFI_STATE_AP_IDLE,
  WIFI_STATE_AP_CONNECTED,
  WIFI_STATE_ERROR
} wifi_state_e;

class Wifi
{
    private:
      String _ap_ssid;
      String _ap_pass;

      wifi_state_e state;

    public:
      Wifi();

      void setup_AP();
      void loop();

      static String getMAC();
};

#endif /* WIFI_H_ */
