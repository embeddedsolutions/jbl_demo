var indexSectionsWithContent =
{
  0: "bcdghijlmprstwy",
  1: "bcdlpr",
  2: "bcjlp",
  3: "bcilpst",
  4: "bcglprstwy",
  5: "bcls",
  6: "bghlprswy",
  7: "chm",
  8: "j"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

