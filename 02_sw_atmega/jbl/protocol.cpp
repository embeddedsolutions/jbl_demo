#include "Protocol.hpp"

                   
/* For detailed description check the included header file */
uint8_t Protocol::process(uint8_t *pBuf) {

  uint8_t readByte;
  static uint8_t dataLen;
  static uint8_t tmpDataLen;
  static uint8_t idx;

  if (currState != STATE::HNDSHAKE_RESP) {
    if ((!Serial.available()) || (!pBuf))
      return 0;
  }
        
  switch(currState) {

    /* Receive handshake */
    case STATE::HNDSHAKE_RECV: {
      readByte = Serial.read();
      if (CMD::HNDSHAKE == readByte) {
        Serial.println("[RX] handshake REQ");
        currState = STATE::HNDSHAKE_RESP;
      }
      break;
    }

    /* Handshake response */
    case STATE::HNDSHAKE_RESP: {
      const uint8_t hndshakeRespLen = 0x04;
      const uint8_t dataLen = 0x02;
      const uint8_t hndshake_resp[] = {HNDSHAKE, dataLen, 0x00, 0xb5};

      Serial.print("[TX] handshake RESP: ");
      Serial.write(hndshake_resp, hndshakeRespLen);
      Serial.println();

      handshakeFinished = true;
      currState = STATE::SET_PWM_ID_RECV;
      break;
    }

    /* Receive command ID */
    case STATE::SET_PWM_ID_RECV: {
      readByte = Serial.read();
      
      if (CMD::SET_PWM == readByte) {
        Serial.print("[RX] packet: 0x");
        Serial.print(readByte, HEX);
        Serial.print(" ");

        idx = 0x00;
        dataBuf[idx++] = readByte;
        currState = STATE::LEN_RECV;
      }
      break;
    }

    /* Receive data length */
    case STATE::LEN_RECV: {
      dataLen = Serial.read();
      tmpDataLen = dataLen;

      Serial.print("0x");
      Serial.print(tmpDataLen, HEX);
 
      if (tmpDataLen != MAX_PACKET_LEN) {
        Serial.println();
        Serial.print("[RX] decoding length error. Expected len: 0x");
        Serial.print(MAX_PACKET_LEN, HEX);
        Serial.print(" received len: 0x");
        Serial.println(tmpDataLen, HEX);
        currState = STATE::SET_PWM_ID_RECV;
      }
      else {
        dataBuf[idx++] = dataLen;
        currState = STATE::PAYLOAD_RECV;
      }

      break;
    }
    
    /* Receive data and verify the checksum */
    case STATE::PAYLOAD_RECV: {
      if ((tmpDataLen ) > 0) {
        readByte = Serial.read();

        if (readByte > MAX_PAYLOAD_BYTE_VAL) {
          Serial.println(" -> payload error");
          currState = STATE::SET_PWM_ID_RECV;
          return 0;
        }

        Serial.print(" 0x");
        Serial.print(readByte, HEX);
        Serial.print(" ");

        dataBuf[idx++] = readByte;
        tmpDataLen--;
      }

      if (tmpDataLen == 0) {
        Serial.println();
        currState = STATE::SET_PWM_ID_RECV;

        uint8_t rxCrc = TwoBytesIntoUint8(dataBuf[MAX_PACKET_LEN], dataBuf[MAX_PACKET_LEN + 0x01u]);

        if (!crcIsValid(dataBuf, dataLen, rxCrc)) {
          Serial.println("[RX] crc error");
          return 0;
        }
 
        memcpy(pBuf, &dataBuf[HEADER_OFFSET], dataLen - CHECKSUM_LEN);
        return dataLen - CHECKSUM_LEN;
      }
   
      break;
    }
  }

  return 0;
}

/* For detailed description check the included header file */
uint8_t Protocol::TwoBytesIntoUint8(uint8_t fByte, uint8_t sByte) {
  return ((fByte << 4) | sByte);
}

/* For detailed description check the included header file */
uint16_t Protocol::ThreeBytesIntoUint16(uint8_t fByte, uint8_t sByte, uint8_t tByte) {
  return ((fByte << 8) | (sByte << 4) | tByte);
}

/* For detailed description check the included header file */
bool Protocol::crcIsValid(uint8_t *pBuf, uint8_t len, uint8_t rxCrc) {

  if ((!pBuf) || (len <= 0u))
    return false;

  uint8_t calculatedCrc = crc.crc8xFast(pBuf, len);
  return (calculatedCrc == rxCrc);
}

/* For detailed description check the included header file */
bool Protocol::sendButtonsState(bool sw1, bool sw2, bool sw3, uint8_t randomNr) {
  if ((!handshakeFinished) || (!Serial.availableForWrite()))
    return false;

  const uint8_t bufLen = 0x09u;
  const uint8_t dataLen = bufLen - CHECKSUM_LEN;

  const uint8_t btnDataOffset = 0x02;
  const uint8_t randomNrOffset = btnDataOffset + 0x03u;
  const uint8_t crcOffset = randomNrOffset + 0x02u;

  uint8_t buf[bufLen] = {CMD::GET_BTN_STATE, dataLen};

  buf[btnDataOffset] = sw1;
  buf[btnDataOffset + 1u] = sw2;
  buf[btnDataOffset + 2u] = sw3;

  buf[randomNrOffset] = (randomNr >> 4) & 0x0F;
  buf[randomNrOffset + 1u] = randomNr & 0x0F;

  uint8_t calculatedCrc = crc.crc8xFast(buf, bufLen - CHECKSUM_LEN);

  buf[crcOffset] = (calculatedCrc >> 4) & 0x0F;
  buf[crcOffset + 1u] = calculatedCrc & 0x0F;

  Serial.print("[TX] button state: ");

  for (int i = 0; i < bufLen; i++) {
    Serial.print(" 0x");
    Serial.print(buf[i], HEX);
  }

  Serial.println();
  Serial.write(buf, bufLen);
  Serial.println();
  return true;
}
