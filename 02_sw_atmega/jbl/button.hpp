#ifndef _BUTTON_HPP_
#define _BUTTON_HPP_

#include <ezButton.h>

/**
 * @brief Class for controll the Buttons
 */
class Btn {
  public:
    enum BTN_ID {
      SW1 = 2, /**<BTN 1 id */
      SW2 = 3, /**<BTN 2 id */
      SW3 = 4  /**<BTN 3 id */
    };

    /**
     * @brief Class constructor
     */
     Btn() : sw1(SW1), sw2(SW2), sw3(SW3) {}

     /**
      * @brief Method for initializing the buttons
      * @param debounceMs - debounce time in miliseconds
      * @retval none
      */
     void init(int debounceMs) {
      sw1.setDebounceTime(debounceMs);
      sw2.setDebounceTime(debounceMs);
      sw3.setDebounceTime(debounceMs);
    }

    /**
     * @brief Method for processing the buttons
     * @details This method has to be called in the main loop
     * @param none
     * @retval none
     */
    void process(void) {
      sw1.loop();
      sw2.loop();
      sw3.loop();
    }

    /**
     * @brief Method for checking whether given button has been pressed
     * @param swId - id of the button
     * @retval bool - true if pressed, otherwise false
     */
    bool isPressed(BTN_ID swId) {
      switch(swId) {
        case SW1:
          return sw1.isPressed();

        case SW2:
          return sw2.isPressed();

        case SW3:
          return sw3.isPressed();

        default:
          return false;
      }
    }

    /**
     * @brief Method for checking whether given button has been released
     * @param swId - id of the button
     * @retval bool - true if released, otherwise false
     */
    bool isReleased(BTN_ID swId) {
      switch(swId) {
        case SW1:
          return sw1.isReleased();

        case SW2:
          return sw2.isReleased();

        case SW3:
          return sw3.isReleased();

        default:
          return false;
      }
    }

  private:
    ezButton sw1;
    ezButton sw2;
    ezButton sw3;
};

#endif /* _BUTTON_HPP_ */
