#ifndef _LED_HPP_
#define _LED_HPP_

#include <Arduino.h>

/**
 * @brief Class for controll the LEDs
 */
class Led {
  public:
    enum LED_ID {
      RED = 9,    /**<Red LED id */
      GREEN = 10, /**<Green LED id */
      BLUE = 11,  /**<Blue LED id */
      YELLOW = 6, /**<Yellow LED id */
      WHITE = 5   /**<White LED id */
    };

  struct duty {
    uint16_t red;     /**<pwm duty value for Red LED */
    uint16_t green;   /**<pwm duty value for Greend LED */
    uint16_t blue;    /**<pwm duty value for Blueed LED */   
    uint16_t white;   /**<pwm duty value for Yellow LED */
    uint16_t yellow;  /**<pwm duty value for White LED */
  };
    
    /**
     * @brief Class constructor
     */
    Led() {
      pinMode(RED, OUTPUT);
      pinMode(GREEN, OUTPUT);
      pinMode(BLUE, OUTPUT);
      pinMode(YELLOW, OUTPUT);
      pinMode(WHITE, OUTPUT);

      analogWrite(RED, 0u);
      analogWrite(GREEN, 0u);
      analogWrite(BLUE, 0u);
      analogWrite(YELLOW, 0u);
      analogWrite(WHITE, 0u);
    }

    /**
     * @brief Method for setting the PWM duty for all LEDs
     * @param ledId id of the LED to be controlled 
     * @param duty (0-2400) it will be aligned to the 0 - 255 range.
     * @retval none
     */
    void setPwm(duty led) {
      analogWrite(RED, unit16PwmtoUint8(led.red));
      analogWrite(GREEN, unit16PwmtoUint8(led.green));
      analogWrite(BLUE, unit16PwmtoUint8(led.blue));
      analogWrite(YELLOW, unit16PwmtoUint8(led.yellow));
      analogWrite(WHITE, unit16PwmtoUint8(led.white));
    }

  private:
    uint8_t unit16PwmtoUint8(uint16_t pwmVal) {
      const uint16_t div = 9;
      const uint16_t maxUint8tVal = 255u;

      uint16_t retVal = (pwmVal / div);
      if (retVal >= maxUint8tVal)
        retVal = maxUint8tVal;
    
      return static_cast<uint8_t>(retVal);
    }
};

#endif /* _LED_HPP_ */
