/**
 * @file jbl.ino
 *
 * @mainpage JBL demo project
 *
 * @section description General description
 * This project realizes bidirectional communication between Arduino and ESP8266
 * The purpose of this project is:
 *  - control PWM of LEDs via ESP module connected with serial port
 *  - send buttons state to the ESP module with serial port
 *  - send randomly generated value to the ESP module
 *  
 * @section protocol Protocol description
 *  The communication protool used in this project is MSBit
 *  Protocol format is as follows:
 *  
 *  - CMD_ID[1b] LEN[1b] DATA[1b * LEN - 2b(crc)] CRC[2b]
 *  - The left byte half of the payload needs to be '0' (0x0A, 0x01 etc.)
 *  
 *  Supported CMD_ID are:
 *  - 0x81 - for handshaking
 *  - 0xF1 - for setting pwm values
 *  - 0xE1 - for sending buttons state with randomly generated number
 *  
 * Examples:
 * - 0xF1 0x14 0x09 0x06 0x00 0x09 0x06 0x00 0x09 0x06 0x00 0x09 0x06 0x00 0x09 0x06 0x00 0x00 0x01 0x04 0x03 0x07 -> set PWM id:0xF1 len:0x14 pwm y:0x960 pwm w:0x960 pwm r:0x960 pwm g:0x960, pwm b:0x960 timeout:0x14(no update background) checksum:0x37
 * - 0xF1 0x14 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x09 0x0E -> set PWM id:0xF1 len:0x14 pwm y:0x00 pwm w:0x00 pwm r:0x00 pwm g:0x00 pwm b:0x00 timeout:0x00(update background) checksum:0x9E 
 * - 0x81 0x44 -> handshake request (only CMD_ID is required) 
 * - 0x81 0x04 0x00 0xb5 -> handshake response (only CMD_ID is required)
 * - 0xE1 0x07 0x01 0x00 0x00 0x0E 0x08 0x0B 0x07 -> send btn state id:0xE1 len:0x07 sw1:0x01 sw2:0x00 sw3:0x00 randomNr:0xE8 checksum:0xB7
 *  
 * @section libraries Libraries
 * - Arduino_ezButton (https://www.arduino.cc/reference/en/libraries/ezbutton/)
 * - Arduino timer (https://github.com/contrem/arduino-timer)
 * - CRC tool (https://www.ghsi.de/pages/subpages/Online%20CRC%20Calculation/index.php?Polynom=100110001&Message=E1+07+00+00+01+08+0B%0D%0A)
 * 
 * @section author Author
 * - Created by Jakub Witowski www.embeddedsolutions.pl
 */

#include "led.hpp"
#include "button.hpp"
#include "protocol.hpp"
#include <arduino-timer.h>

/* Constants */
constexpr int btnDebounceMs = 50; /**<Buttons debounce time */

/* Object declaration */
Led led;                              /**<Led class object */
Btn btn;                              /**<Button class object */
Protocol proto;                       /**<Protocol class object */
auto timer = timer_create_default();  /**<Arduino-timer object */

/* Types declaration */
struct rxParams {
  Led::duty led;    /**<Received LED duty values */
  uint16_t timeout; /**<Received timeout value */
};

struct btnStates {
  bool sw1; /**<Current sw1 state */
  bool sw2; /**<Current sw2 state */
  bool sw3; /**<Current sw3 state */
};

/* Variable declaration */
uint8_t rxBuff[32];       /**<Buffer for received data */
rxParams params;          /**<Received parameters data */
Led::duty background;     /**<LEDs current background pwm duty */
btnStates currBtnState;   /**<Current sw states object */
btnStates prevBtnState;   /**<Previous sw states object */

/* Local function declaration */

/**
 * @brief Method for parsing received data
 * @param none
 * @retval none
 */
static void decodeReceivedData();

/**
 * @brief Method for restoring background colour
 * @details Those method is called by the timer
 * @param *argument - pointer for the additional data
 * @retval bool when timer needs to be retriggered, otherwise false
 */
static bool restoreBackgroundColour(void *argument);

/**
 * @brief Method for handling buttons state
 * @param none
 * @retval none
 */
static void handleButtons(void);


/* Setting up */
void setup() {

  currBtnState.sw1 = false;
  currBtnState.sw2 = false;
  currBtnState.sw3 = false;

  prevBtnState = currBtnState;
  btn.init(btnDebounceMs);

  Serial.begin(115200);
  Serial.println("*** JBL demo ***");
}


/* Main programme loop */
void loop() {
  timer.tick();
  btn.process();
  uint8_t rxLen = proto.process(rxBuff);

  if (rxLen > 0x00) {
    decodeReceivedData();
    led.setPwm(params.led);

    if (params.timeout == 0x00) {

      /* Update background color when received 0x00 as timeout value */
      background = params.led; 
      Serial.println("background colour updated");
    }
    else {
      Serial.print("new colour set for: ");
      Serial.print(params.timeout, DEC);
      Serial.println("ms");

      if (!timer.empty()) {
        Serial.println("timer in progress has been canceled");     
        timer.cancel();
      }
      timer.in(params.timeout, restoreBackgroundColour);
    }
  }

  handleButtons();
}

/* For detailed description check function declaration above */
static void decodeReceivedData(void) {
  const uint8_t w_offset = 0x00;
  const uint8_t y_offset = 0x03;
  const uint8_t r_offset = 0x06;
  const uint8_t g_offset = 0x09;
  const uint8_t b_offset = 0x0c;
  const uint8_t tmout_offset = 0x0f;
  
  params.led.white = proto.ThreeBytesIntoUint16(rxBuff[w_offset], rxBuff[w_offset + 1u], rxBuff[w_offset + 2u]);
  params.led.yellow = proto.ThreeBytesIntoUint16(rxBuff[y_offset], rxBuff[y_offset + 1u], rxBuff[y_offset + 2u]);
  params.led.red = proto.ThreeBytesIntoUint16(rxBuff[r_offset], rxBuff[r_offset + 1u], rxBuff[r_offset + 2u]);
  params.led.green = proto.ThreeBytesIntoUint16(rxBuff[g_offset], rxBuff[g_offset + 1u], rxBuff[g_offset + 2u]);
  params.led.blue = proto.ThreeBytesIntoUint16(rxBuff[b_offset], rxBuff[b_offset + 1u], rxBuff[b_offset + 2u]);
  params.timeout = proto.ThreeBytesIntoUint16(rxBuff[tmout_offset], rxBuff[tmout_offset + 1u], rxBuff[tmout_offset + 2u]);

  Serial.print("w:0x");
  Serial.print(params.led.white, HEX);
  Serial.print(" y:0x");
  Serial.print(params.led.yellow, HEX);

  Serial.print(" r:0x");
  Serial.print(params.led.red, HEX);
  Serial.print(" g:0x");
  Serial.print(params.led.green, HEX);
  Serial.print(" b:0x");
  Serial.print(params.led.blue, HEX);
  
  Serial.print(" tmout:0x");
  Serial.println(params.timeout, HEX);
}

/* For detailed description check function declaration above */
static bool restoreBackgroundColour(void *argument) {
  (void)argument;

  Serial.println("restoring background color");
  led.setPwm(background);

  /* Return false for timer signle shot */
  return false;
}

/* For detailed description check function declaration above */

static void handleButtons(void) {
  if (btn.isPressed(Btn::SW1)) {
    currBtnState.sw1 = true;
  }
  else if (btn.isReleased(Btn::SW1)){
    currBtnState.sw1 = false;
  }

  if (btn.isPressed(Btn::SW2)) {
    currBtnState.sw2 = true;
  }
  else if (btn.isReleased(Btn::SW2)){
    currBtnState.sw2 = false;
  }

  if (btn.isPressed(Btn::SW3)) {
    currBtnState.sw3 = true;
  }
  else if (btn.isReleased(Btn::SW3)){
    currBtnState.sw3 = false;
  }

  if ((currBtnState.sw1 != prevBtnState.sw1) ||
      (currBtnState.sw2 != prevBtnState.sw2) ||
      (currBtnState.sw3 != prevBtnState.sw3)) {

    prevBtnState = currBtnState;
    if (!proto.sendButtonsState(currBtnState.sw1, currBtnState.sw2, currBtnState.sw3, random(0xFF)))
      Serial.println("cannot send button update - handshake in progress");
  }
}
