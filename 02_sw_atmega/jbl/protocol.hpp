#ifndef _PROTOCOL_HPP_
#define _PROTOCOL_HPP_

#include <Arduino.h>
#include "crc8.hpp"

/** Max allowed packet length */
#define MAX_PACKET_LEN (0x14u)

/** Checksum length */
#define CHECKSUM_LEN (0x02u)

/** Command ID + packet LEN offset */
#define HEADER_OFFSET (0x02u)

/** Payload's max single byte value */
#define MAX_PAYLOAD_BYTE_VAL (0x0Fu)

/**
 * @brief Class for parsing the protocol
 */
class Protocol {
  public:
    enum STATE {
      HNDSHAKE_RECV,   /**<State for receiving handshake */
      HNDSHAKE_RESP,   /**<State for sending handshake */
      SET_PWM_ID_RECV, /**<State command ID received */
      LEN_RECV,        /**<State length received */
      PAYLOAD_RECV     /**<State payload received */
    };
 
    enum CMD {
      HNDSHAKE = 0x81,       /**<Handshake command id */
      SET_PWM = 0xF1,       /**<Set PWM command id */
      GET_BTN_STATE = 0xE1  /**<Send Buttons state command id */
    };

    /**
     * @brief Class constructor
     */
    Protocol() : currState(HNDSHAKE_RECV), handshakeFinished(false) {}

    /**
     * @brief Method for processing the protocol
     * @details This method has to be called in the main loop
     * @param pBuf - pointer to the buffer for received data
     *               Size of the buffer shall be at least @ref maxPacketLen 
     * @retval length of received data
     */
    uint8_t process(uint8_t *pBuf);

    /**
     * @brief Method for sending buttons state with random numer
     * @param sw1 - state of sw1
     * @param sw1 - state of sw2
     * @param sw1 - state of sw3
     * @param randomNr - uint8_t generated random number
     * @retval true if success, otherwise false
     */
    bool sendButtonsState(bool sw1, bool sw2, bool sw3, uint8_t randomNr);

    /**
     * @brief Method for converting 2 bytes into uint8_t
     * @param fByte - first byte of the data
     * @param sByte - second byte of the data
     * @retval converted uint8_t value
     */
    uint8_t TwoBytesIntoUint8(uint8_t fByte, uint8_t sByte);

    /**
     * @brief Method for converting 3 bytes into uint16_t
     * @param fByte - first byte of the data
     * @param sByte - second byte of the data
     * @param tByte - third byte of the data
     * @retval converted uint16_t value
     */
    uint16_t ThreeBytesIntoUint16(uint8_t fByte, uint8_t sByte, uint8_t tByte);

  private:
    Crc8 crc;
    STATE currState;
    bool handshakeFinished;
    uint8_t dataBuf[MAX_PACKET_LEN + CHECKSUM_LEN];
    bool crcIsValid(uint8_t *pBuf, uint8_t len, uint8_t rxCrc);
};

#endif /* _PROTOCOL_HPP_ */
